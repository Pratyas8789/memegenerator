import React from 'react'

export default function MemeComponent() {
    const [meme, setMeme] = React.useState({
      topText: "",
      bottomText: ""
    })
    const [memeImage, setMemeImage] = React.useState("https://i.imgflip.com/1wz1x.jpg")
  
    let MemeData;
    React.useEffect(() => {
      fetch("https://api.imgflip.com/get_memes")
        .then((response) => {
          return response.json()
        })
        .then((data) => {
          MemeData = data
        })
        .catch((error) => {
          console.error(error.message);
        })
    })
  
    const getMemeImage = () => {
      const memeArray = MemeData.data.memes
      const index = Math.floor(Math.random() * memeArray.length)
      setMemeImage(memeArray[index].url)
    }
    function handleChange(event) {
      const { name, value } = event.target
      setMeme((meme) => ({
        ...meme,
        [name]: value
      }))
    }
    return (
      <div className='memeComponent'>
        <div className="inputDiv">
          <input type="text" name="topText" id="" onChange={handleChange} value={meme.topText} placeholder='Top text' />
          <input type="text" name="bottomText" id="" onChange={handleChange} value={meme.bottomText} placeholder='Bottom text' />
        </div>
        <button onClick={getMemeImage} >Get a new meme image 🖼</button>
        <div className="meme">
          <img src={memeImage} alt="" srcset="" className='memeImage' />
          <h2 className='topText'>{meme.topText}</h2>
          <h2 className='bottomText'>{meme.bottomText}</h2>
        </div>
      </div>
    )
  }
