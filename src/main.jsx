import React from 'react'
import ReactDOM from 'react-dom/client'
import MemeComponent from './component/MemeComponent'
import Navbar from './component/Navbar'
import './index.css'

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <Navbar/>
    <MemeComponent/>
  </React.StrictMode>,
)
